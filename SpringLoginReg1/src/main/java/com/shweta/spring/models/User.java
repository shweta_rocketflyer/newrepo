package com.shweta.spring.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Version;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Property;

@Repository
@Entity(value="User_info")
public class User {
	
	@Id private ObjectId id;	
	@Version private long version;
	@Property private String firstname;
	@Property private String lastname;
	@Property private String email;
	@Property private String password;
	@Property private String username;
	
	public User(ObjectId id, String firstname, String lastname, String email, String password, String username) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.username = username;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public String toString() {
		return "User [password=" + password + ", username=" + username + "]";
	}
	
	
}
