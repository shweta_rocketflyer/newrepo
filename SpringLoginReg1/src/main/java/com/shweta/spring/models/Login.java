package com.shweta.spring.models;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Version;
import org.springframework.stereotype.Repository;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Property;

@Entity(value="Login_info")
@Repository
public class Login {	
		
		@Property private String password;
		@Property private String username;
		
		
		public Login() {
			super();
			// TODO Auto-generated constructor stub
		}
		public Login(String password, String username) {
			super();
			this.password = password;
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
	

}
