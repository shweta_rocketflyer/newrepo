package com.shweta.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shweta.dao.RegisterDAO;
import com.shweta.dto.User;

//@WebServlet("/Register")
public class Register extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");
        String pass = request.getParameter("psw");
        String uname = request.getParameter("username");
        
        
        
        User user=new User(null, fname,lname,email,uname,pass);
        
        
        user.setFirstname(fname);
        user.setLastname(lname);
        user.setEmail(email);
        user.setPassword(pass);
        user.setUsername(uname);
       
        
        RegisterDAO registerdao = new RegisterDAO();
        String message= registerdao.registerUser(user);
        
        if(message.equals("SUCCESS"))   //On success, you can display a message to user on Home page
        {
        	request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
        else   //On Failure, display a meaningful message to the User.
        {
           request.setAttribute("errMessage",message );
           request.getRequestDispatcher("/register.jsp").forward(request, response);
        }
	}
        
        protected void doget(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    		// TODO Auto-generated method stub
    	
            String fname = request.getParameter("fname");
            String lname = request.getParameter("lname");
            String email = request.getParameter("email");
            String pass = request.getParameter("psw");
            String uname = request.getParameter("username");
            
            
            
            User user=new User(null, fname,lname,email,uname,pass);
            
            
            user.setFirstname(fname);
            user.setLastname(lname);
            user.setEmail(email);
            user.setPassword(pass);
            user.setUsername(uname);
           
            
            RegisterDAO registerdao = new RegisterDAO();
            String message= registerdao.registerUser(user);
            
            if(message.equals("SUCCESS"))   //On success, you can display a message to user on Home page
            {
            	request.getRequestDispatcher("/login.jsp").forward(request, response);
            }
            else   //On Failure, display a meaningful message to the User.
            {
               request.setAttribute("errMessage",message );
               request.getRequestDispatcher("/register.jsp").forward(request, response);
            }
            
        
     
		
	}
	

		

}
