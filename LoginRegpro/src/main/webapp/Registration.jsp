<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<form action="register" method="post">
  
    <h1>Sign Up</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>
    
    <label for="firstname"><b>First Name</b></label>
    <input type="text" placeholder="Enter First Name" name="fname" required>
	<label for="lastname"><b>Last Name</b></label>
    <input type="text" placeholder="Enter Last Name" name="lname" required>
	<br><br>
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" required>
	<br><br>
	<label for="username"><b>Username</b></label>
    <input type="text" placeholder="Enter username" name="username" required>
	<br><br>
    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>
	<br><br>
    <label for="psw-repeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
    <br><br>
    <input type="submit" value="Signup">
    </form>


</body>
</html>