package com.shweta.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.mongodb.morphia.Morphia;

import com.shweta.dao.LoginDAO;
import com.shweta.dao.MongoConnection;
import com.shweta.dao.RegisterDAO;
import com.shweta.dto.Login;
import com.shweta.dto.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Loginservlet extends HttpServlet {
	
   
	@SuppressWarnings("deprecation")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
		// TODO Auto-generated method stub
		
        String pass = request.getParameter("password");
        String uname = request.getParameter("username");
        
        
        
        
        Login login =new Login();
        
        login.setPassword(pass);
        login.setUsername(uname);
        
        LoginDAO logindao = new LoginDAO();        
        String message=logindao.authenticateUser(login);
        
        if(message.equalsIgnoreCase("SUCCESS"))   //On success, you can display a message to user on Home page
        {
        	request.getRequestDispatcher("/welcome.jsp").forward(request, response);
        }
        else   //On Failure, display a meaningful message to the User.
        {
           request.setAttribute("errMessage",message );
           request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
		
	}

	

}
