package com.shweta.dao;

import java.util.Iterator;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;
import com.shweta.dto.Login;
import com.shweta.dto.User;

public class LoginDAO {
	
	@SuppressWarnings("unused")
	public String authenticateUser( Login login)
    {
		String uname=login.getUsername();
		String pass=login.getPassword();
		
		MongoConnection ms = MongoConnection.getInstance();
		
		
		Query<User> query = ms.getDatastore().createQuery(User.class)
                .filter("username ", uname)
                .filter("password", pass);
		
		List<User> u = query.asList();
		
		if(u.isEmpty())
		{
			return "Invalid Input";
		}
		else
		{
			return "success";
		}
		
	

    }

}
